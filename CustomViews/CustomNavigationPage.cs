﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using NavigationPage = Xamarin.Forms.NavigationPage;

namespace CommonUI.CustomViews
{
    public class CustomNavigationPage : NavigationPage
    {
        public CustomNavigationPage(ContentPage Page) : base(Page)
        {
            On<iOS>().SetHideNavigationBarSeparator(true);
            SetDynamicResource(BackgroundColorProperty, "BackgroundColor");
            SetDynamicResource(BarBackgroundColorProperty, "AccentColor1");
            SetDynamicResource(BarTextColorProperty, "TextColor");
            SetDynamicResource(IconColorProperty, "TextColor");
        }
    }
}
