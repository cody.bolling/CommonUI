﻿using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace CommonUI.CustomViews
{
    public class GradientBackground : PancakeView
    {
        public GradientBackground()
        {
            Margin = 0;
            Padding = 0;
            CornerRadius = 0;
            BackgroundGradientAngle = 0;

            BackgroundGradientStops = new GradientStopCollection
            {
                new GradientStop { Color = (Color)Application.Current.Resources["AccentColor3"], Offset = 0 },
                new GradientStop { Color = (Color)Application.Current.Resources["AccentColor4"], Offset = 0.7F },
                new GradientStop { Color = (Color)Application.Current.Resources["AccentColor5"], Offset = 1 }
            };
        }
    }
}

